from flask import Flask, render_template, jsonify
import nmap

app = Flask(__name__)

def scan_reseau(plage_ip):
    scanner = nmap.PortScanner()
    results = scanner.scan(plage_ip, arguments="-sS")
    scan_results = {}

    for host in results['scan'].keys():
        if results['scan'][host]['status']['state'] == "up":
            scan_results[host] = {}
            if 'tcp' in results['scan'][host]:
                for port in results['scan'][host]['tcp'].keys():
                    if results['scan'][host]['tcp'][port]['state'] == "open":
                        scan_results[host][port] = "ouvert"

    return scan_results

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/lancer_scan')
def lancer_scan():
    plage_ip = "172.16.3.0/24"
    scan_results = scan_reseau(plage_ip)
    return jsonify(scan_results)

if __name__ == '__main__':
    app.run(debug=True)
